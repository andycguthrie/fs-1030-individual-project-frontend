import React from 'react'
import './App.css'
import Navigation from './components/shared/Navigation'
import Footer from './components/shared/footer'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/pages/Home'
import About from './components/pages/About'
import Contact from './components/pages/Contact'
import Login from './components/pages/Login'
import Resume from './components/pages/Resume'
import Listing from './components/pages/Listing'
import Portfolio from './components/pages/Portfolio'
import PortfolioItem from './components/pages/PortfolioItem'
import PrivateRoute from './components/shared/PrivateRoute'
// import EditPortfolio from './components/admin/edit_portfolio'
import AddPortfolio from './components/admin/add_portfolio'

function App() {
  return (
   <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/addPortfolio" component={AddPortfolio} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/portfolio" component={Portfolio} />
          <Route exact path="/resume" component={Resume}/> 
          <Route exact path="/portfolio/:id" component= {PortfolioItem} />
          <PrivateRoute path="/submissions">
            <Listing />
          </PrivateRoute>
        </Switch>
        <Footer />  
    </BrowserRouter>
  )
}

export default App;
