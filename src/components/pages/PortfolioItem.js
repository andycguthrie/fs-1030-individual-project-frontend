import React, { useState, useEffect } from "react";
import { Container, Button } from 'reactstrap';


const PortfolioItem = (props) => {
  const [portfolio, setPortfolio] = useState([]);


  let id = props.match.params.id;
  
  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`http://localhost:4000/api/portfolio/${id}`);
      res
        .json()
        .then((res) => setPortfolio(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, [id]);


  return (
    <Container className='main'>
           
    {portfolio.map((portfolioObject)=>(
        <div key={portfolioObject}>
 
 <h1 className="xlarge-pages">{portfolioObject.project}</h1>
<h2 className="xlarge-pages">{portfolioObject.client}</h2>
<Button color="secondary">{portfolioObject.tag1}</Button>{' '},
<Button color="secondary">{portfolioObject.tag2}</Button>{' '}
<Button color="secondary">{portfolioObject.tag3}</Button>{' '}
<h1 className="xlarge-pages">{portfolioObject.name}</h1>
 <div>
     <img src={portfolioObject.photo} alt={portfolioObject.name} width="100%" height="auto" />
 </div>   



        </div>
    ))}
</Container>
  );
};
export default PortfolioItem;



