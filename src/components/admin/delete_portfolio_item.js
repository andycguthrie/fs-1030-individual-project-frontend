import React, {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { Container, Row, Col, Button} from 'reactstrap'


const PortfolioEdit = () => {
    const [portfolio, setPortfolio] = useState(null);
    const history = useHistory();
   
       
    
        // useEffect(() => {
        //     // DELETE request using fetch with async/await
        //     async function deletePost() {
        //         await fetch('http://localhost:4000/api/portfolio/${id}', { method: 'DELETE' });
        //         setStatus('Delete successful');
        //     }
    
        //     deletePost();
        // }, []);
    
        // return (
        //     <div className="card text-center m-3">
        //         <h5 className="card-header">DELETE Request with Async/Await</h5>
        //         <div className="card-body">
        //             Status: {status}
        //         </div>
        //     </div>
        // );
    
    useEffect(() => {
        async function fetchData() {
          const res = await fetch("http://localhost:4000/api/portfolio");
          res
            .json()
            .then((res) => setPortfolio(res))
            .catch((err) => console.log(err));
        }
        fetchData();
      }, []);


     
      
      const handleDelete = (e, portfolio) => {
        e.preventDefault();
        let id = portfolio.id;
        fetch(`/api/portfolio/${id}`, {
            method: "delete",
            headers: {
                Accept: "application/json",
                "Content-type": "application/json"
            },
            body: JSON.stringify(portfolio)
        }).then((response) => response.json());
        history.push('/api/portfolio')
    }

    return (
        <Container className='main'>
           <h1>Portfolio</h1>
           {portfolio.map((portfolioObject)=>(
               <div key={portfolioObject}>
         <Row className="my-5">
            <Col lg="7">
            <img src={portfolioObject.photo} alt="" width="100%" height="auto"/>
            </Col>
            <Col lg="5">
             <Button onClick={handleDelete}>Delete</Button>
            {/* <h1 onClick={(e) => PortfolioRoute(e, portfolioObject)} className="xlarge-pages"> {portfolioObject.client}</h1> */}

    </Col>
    </Row>

    

        

               </div>
           ))}
    </Container>
    )
}

export default PortfolioEdit