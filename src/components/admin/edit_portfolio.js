import React, {useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import { Container, Row, Col} from 'reactstrap'
import Portfolio from "../pages/Portfolio";

const PortfolioForm = props => {
    const [portfolio, setportfolio] = useState(props.portfolio)
  
    const submit = e => {
      e.preventDefault()
      fetch('http://localhost:4000/api/portfolio/${id}', {
        method: 'PUT',
        body: JSON.stringify({ portfolio }),
        headers: { 'Content-Type': 'application/json' },
      })
        .then(res => res.json())
        .then(json => setportfolio(json.portfolio))
    }

    useEffect(() => {
        // PUT request using fetch with async/await
        async function updatePost() {
           const requestOptions = {
              method: 'PUT',
              headers: {
                 'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                 title: 'React Hooks PUT Request Example'
              })
           };
           const response = await fetch('http://localhost:4000/api/portfolio/${id}', requestOptions);
           const data = await response.json();
        //    setPostId(data.id);
        }
     
        updatePost();
     }, []);

  
    return (
      <form onSubmit={submit}>
          
        <input
          type="text"
          name="portfolio[name]"
          value={portfolio.project}
          onChange={e => setportfolio({ ...portfolio, name: e.target.value })}
        />
        {portfolio.errors.name && <p>{portfolio.errors.name}</p>}
  
        <input
          type="email"
          name="portfolio[email]"
          value={portfolio.email}
          onChange={e => setportfolio({ ...portfolio, email: e.target.value })}
        />
        {portfolio.errors.name && <p>{portfolio.errors.name}</p>}
  
        <input type="submit" name="Sign Up" />
      </form>
    )
  }
  
  export default PortfolioForm