import React, { useState } from "react";
import { Container, Row, Col, Button, Form, Label, FormGroup, Input} from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useHistory } from "react-router";

const AddPortfolioForm = () => {

  let history = useHistory();

  const [values, setValues] = useState({
      project: "",
      client:"",
      photo: "",
      description: "",
      tag1: "",
      tag2: "",
      tag3: ""
  });

  const handleProjectChang = e => {
      setValues({...values, project: e.target.value})
  }
  const handleClientChange = e => {
      setValues({...values, client: e.target.value})
  }
  const handlePhotoChange = e => {
      setValues({...values, photo: e.target.value})
  }
  const handleDescriptionChange = e => {
      setValues({...values, description: e.target.value})
  }
  const handleTag1Change = e => {
      setValues({...values, tag1: e.target.value})
  }

  const handleTag2Change = e => {
    setValues({...values, tag2: e.target.value})
}

const handleTag3Change = e => {
  setValues({...values, tag3: e.target.value})
}
  const clearState = () => {
      setValues({...values});
  };


  const formSubmit = async event => {
      event.preventDefault()
      const response = await fetch('http://localhost:4000/api/portfolio', {
          method: 'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
          body: JSON.stringify(values)
      })
      const payload = await response.json()
      if (response.status >= 400) {
          alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid}`)
      } else {
          alert(`Thank you! File has been created with id: ${payload.id}`);
      }
      clearState();
      history.push("/api/portfolio");
  }

// const AddPortfolioForm = () => {
//   const [newPortfolio, setNewPortfolio] = useState({ project: "", client: "", photo: "", description: "", tag1: "", tag2: "", tag3: ""});


//   const handleChange = (event) => {
//     setNewPortfolio((prevState) => ({
//       ...prevState,
//       [event.target.name]: event.target.value,
//     }));
//   };

//   const handleSubmit = () =>{
//      fetch("http://localhost:4000/api/portfolio", {
//        method: "post",
//        headers: {
//          Accept: "application/json",
//          "Content-Type": "application/json",
//        },

//        //make sure to serialize your JSON body
//        body: JSON.stringify(newPortfolio),
//      }).then((response) => response.json());
 
//   }

  return (
    <Container className='main'>
    <Form className="form" onSubmit={formSubmit}>
      <FormGroup>
        <Label>Project</Label>
        <Input
         type="text"
         name="project"
         placeholder="Enter Project Name"
         onChange={handleProjectChang}
        />
      </FormGroup>
      <FormGroup>
        <Label>Client
        </Label>
        <Input
           type="text"
           name="client"
           placeholder="Enter Client Name"
           onChange={handleClientChange}
          
        />
      </FormGroup>
      <FormGroup>
        <Label>Image</Label>
        <Input
           type="text"
           name="photo"
           placeholder="Past URL for photograph"
           onChange={handlePhotoChange}
          
        />
      </FormGroup>
      <FormGroup>
        <Label>Description</Label>
        <Input
           type="text"
           name="description"
           placeholder="Enter project description"
           onChange={handleDescriptionChange}
          
        />
      </FormGroup>
      <FormGroup>
        <Label>Tag 1</Label>
        <Input
           type="text"
           name="tag1"
           placeholder="Enter Your Role"
           onChange={handleTag1Change}
          
        />
      </FormGroup>
      <FormGroup>
        <Label>Tag 2</Label>
        <Input
           type="text"
           name="tag2"
           placeholder="Enter Your Role"
           onChange={handleTag2Change}
          
        />
      </FormGroup>
      <FormGroup>
        <Label>Tag 3</Label>
        <Input
           type="text"
           name="tag3"
           placeholder="Enter Your Role"
           onChange={handleTag3Change}
          
        />
      </FormGroup>
    <Button>Submit</Button>
  </Form>
  </Container>
);
}


export default AddPortfolioForm;
